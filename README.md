#### Running the program

npm i

npm start in folder json-server

run app.html in liveserver

because of modules and cors not allowing to access local js files in browser it is not allowing you to directly open app.html without liveserver

#### Running the tests

npm test app.test.js

#### Challenges

I found that unit testing some parts of the code was difficult, because many of the functions did dom manipulation. Could maybe be fixed by inserting the dom with a template literal before the tests, however did not make this work. So the tests are only testing some parts of the code where it is not manipulating the DOM.

#### Extras

- [x] implemented fetch function for laptops with json-server
- [x] implemented testing with jest

#### Checklist of assignment requirments

- [x] Manipulate HTML using JavaScript
- [x] Use JavaScript arrays
- [x] Bind events to HTML elements
- [x] Listen for events from HTML elements
- [x] Write custom CSS to style HTML elements and layouts

#### Grading Criteria: The following main elements must be present in your solution:

- [ ] Working version of the assignment hosted on Heroku (OPTIONAL)
- [x] All requested features must be present and working
- [x] A Gitlab repository showing the work you have

**1. The Bank**

- [x] Show balance
- [x] Show outstanding Loan Value(only visible after taking a loan)
- [x] Get loan, button with prompt popup that allows you to enter loan amount
  - [x] Cannot get loan more than double bank balance
  - [x] You cannot get more than one bank loan before buying a computer.
  - [x] Once you have a loan, you must pay it back BEFORE getting another loan

**2. Work**

- [x] Pay amount earned by "working" NOT part of bank balance.
- [x] The bank button must transfer the money from your Pay balance, to your Bank balance. Remember to reset your pay once you transfer.
  - [x] If you have an outstanding loan, 10% of your salary MUST first be deducted and transferred to the outstanding Loan amount
  - [x] The balance after the 10% deduction may be transferred to your bank account
- [x] The work button must increase your Pay balance by 100 on each click.
- [x] Once you have a loan, a new button should appear. Upon clicking this button, the full value of your current Pay amount should go towards the outstanding loan and NOT your bank account.

**3. Laptops**

- [x] Select box with atleast 4 different laptops, unique name, price, description, feature list and image link. Update user interface with the correct information for the selected laptop.
- [x] Feature list displayed at laptop selection
- [x] Large box at the bottom with the laptop info.
  - [x] Buy button validates if you have sufficent funds.
  - [x] Money deducted from bank if succesfull and message that says you are the new owner
  - [x] Message that says you have insufficient funds if you cannot afford the laptop.
