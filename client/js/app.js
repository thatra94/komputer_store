import { fetchComputers } from './api.js'

const feauturesBox = document.querySelector('#features-box')

const bankBalanceText = document.querySelector('#bank-balance-text')
const bankLoanText = document.querySelector('#bank-loan-text')
const workSalaryText = document.querySelector('#work-salary-text')

const getLoanButton = document.querySelector('#get-loan-button')
const BankButton = document.querySelector('#bank-button')
const WorkButton = document.querySelector('#work-button')
const buyButton = document.querySelector('#buy-button')
const payLoanButton = document.querySelector('#pay-loan-button')


const computerSelect = document.querySelector('#computer-select')
const computerInfoBox = document.querySelector('#computer-info')
const computerImage = document.querySelector('#computer-image')

export let bankBalance = 0
export let loan = 0
export let salary = 0

let computerArray = []
    
//Render starting data
window.addEventListener('DOMContentLoaded', () => {
    render()
    createEventListeners()
})
async function render() {
    await fillComputerArray()
    updateInfo()
}

function updateInfo() {
    showBankInfo()
    showComputerInfo()
    showLoankInfo()
    showWorkInfo()
    showComputerFeatures()
}

function createEventListeners() {
    getLoanButton.addEventListener('click', getLoan) 
    BankButton.addEventListener('click', () => {
        transferSalaryToBank()
        updateInfo()
    })
    WorkButton.addEventListener('click', () => {
        salary += 100
        updateInfo()
    })
    computerSelect.addEventListener('change', () => {
        showComputerFeatures()
        showComputerInfo()
    })
    buyButton.addEventListener('click', buyItem)
    payLoanButton.addEventListener('click', () => {
        transferSalaryToLoan()
        updateInfo()
    })
}

async function fillComputerArray() {
    try {
        computerArray = await fetchComputers()
        populateComputerSelect()
        computerSelect.disabled = false
    } catch (e) {
        console.log('This is an error: ', e.message);
    }
}

function showBankInfo() {
    let bankInfo = `Current balance ${bankBalance} kr`
    bankBalanceText.innerHTML = bankInfo
}

function showLoankInfo() {
    let loanInfo = `Current loan ${loan} kr`
    bankLoanText.innerHTML = loanInfo
}

function showWorkInfo() {
    let workInfo = `Current salary ${salary} kr`
    workSalaryText.innerHTML = workInfo
}

function getLoan() {

    if (0 < loan) {
        alert('You have to payback your loan before getting another loan')
        return
    }

    let loanValue = prompt('Enter loan value')
    if (loanValue) {
    checkLoanValue(loanValue)
    }

    if (bankBalance * 2 < loanValue) {
        alert('Loan too high, insufficient funds')
    }
}

function checkLoanValue(loanValue) {

      if (loanValue < bankBalance * 2 && loan === 0) {
        alert(`${loanValue} kr granted in loan`)
        loan = loanValue
        bankBalance += parseInt(loanValue)
        updateInfo()
          bankLoanText.style.display = "block"
          payLoanButton.style.visibility = "visible"
        return loanValue 
    }
    return false
}

export function transferSalaryToBank() {
    if (loan === 0) {
        bankBalance += salary
    }
    deductSalaryForLoan()
    salary = 0
}

export function deductSalaryForLoan() {
    if (loan !== 0) {
        let salaryDeduction = salary / 10
        if (loan < salaryDeduction) {
            salary -= loan
            bankBalance += salary
            loan = 0
            bankLoanText.style.display = "none"
        } else {
            loan -= salaryDeduction
            salary -= salaryDeduction
            bankBalance += salary
        }
    }
}

export function transferSalaryToLoan() {
    if (salary > loan) {
        salary -= loan
        loan = 0
        payLoanButton.style.visibility = "hidden"
        bankLoanText.style.display = "none"
    } else {
        loan -= salary
        salary = 0
    }
}

function populateComputerSelect() {
    console.log(computerArray)
    computerArray.forEach(computer => {
        let newOption = document.createElement('option')
        let optionHtml = computer.name
        newOption.innerHTML = optionHtml
        computerSelect.appendChild(newOption)
    });
}

function showComputerFeatures() {
    feauturesBox.innerHTML = ''
    let selected = computerArray[computerSelect.selectedIndex]
    selected.specs.forEach(spec => {
        let computerFeatures = `
        <h4>${spec}<h4>
        `
        feauturesBox.innerHTML += computerFeatures     
    })
}

function showComputerInfo() {
    let selectedIndex = computerSelect.selectedIndex
    let image = `
        <img src="${computerArray[selectedIndex].image}" alt ="computer" width="300";
        height="300"></img>¨
    `
    computerImage.innerHTML = image
    let info = `
        <h3>${computerArray[selectedIndex].name}<h3>
        <h3>${computerArray[selectedIndex].price} Kr</h3>
        <h3>${computerArray[selectedIndex].description}</h3>
        <h3>Rating: ${computerArray[selectedIndex].rating}</h3>
        `
    computerInfoBox.innerHTML = info
}

function buyItem() {
    let selected = computerArray[computerSelect.selectedIndex]
    if (selected.price > bankBalance) {
        alert('Insufficient funds')
        return
    } 
        bankBalance -= selected.price
        alert(`Successfully bought ${selected.name}`)
        updateInfo()
}

//Setters for testing purposes
export function setLoan(loanAmount) {
    loan = loanAmount
}

export function setSalary(salaryAmount) {
    salary = salaryAmount
}

export function setBalance(balanceAmount) {
    bankBalance = balanceAmount
}
