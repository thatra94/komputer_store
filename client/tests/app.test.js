import * as app from '../js/app.js'

afterEach(() => {
    app.setLoan(0)
    app.setSalary(0)
    app.setBalance(0)
});

describe('Bank and work calculations', () => {
    
    test('should deduct from salary to loan', () => {
        app.setLoan(10)
        app.setSalary(10)
        app.deductSalaryForLoan()
        expect(app.loan).toEqual(9)
    })

        test('should transfer salary to bank', () => {
        app.setLoan(0)
        app.setSalary(100)
        app.transferSalaryToBank()
        expect(app.bankBalance).toEqual(100)
    })
        
    test('should transfer salary to loan', () => {
        app.setSalary(10)
        app.setLoan(100)
        app.transferSalaryToLoan()
        expect(app.loan).toEqual(90)
        expect(app.salary).toEqual(0)

    })
        
})
